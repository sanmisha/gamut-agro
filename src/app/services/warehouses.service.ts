import { Injectable } from '@angular/core';
import { HttpService } from './http/http.service';

@Injectable({
  providedIn: 'root'
})
export class WarehousesService {
  headers = {
    Accept: 'application/json',
  };

  constructor(private http: HttpService) { }

  getWarehousesList() {
    return this.http.get('warehouses', '', this.headers);
  }

  addWarehouses(data) {
    return this.http.post('addWarehouses', data, this.headers);
  }

  editWarehouses(data, id) {
    return this.http.post('editWarehouses', data, this.headers, `/${id}`);
  }
}
