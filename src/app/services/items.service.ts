import { Injectable } from '@angular/core';
import { HttpService } from './http/http.service';

@Injectable({
  providedIn: 'root',
})
export class ItemService {
  headers = {
    Accept: 'application/json',
  };
  constructor(private http: HttpService) {}

  getItemsList() {
    return this.http.get('items', '', this.headers);
  }

  addItems(data) {
    return this.http.post('addItems', data, this.headers);
  }

  editItems(data, id) {
    return this.http.post('editItems', data, this.headers, `/${id}`);
  }

 }
