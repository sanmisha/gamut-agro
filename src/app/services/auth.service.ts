import { Injectable } from '@angular/core';
import { HttpService } from './http/http.service';
@Injectable({
  providedIn: 'root'
})
export class AuthService {
  public headers: {};
  constructor(private httpService: HttpService) {
    this.headers = {
      'Content-Type': 'application/json',
      Accept: 'application/json',
  };
   }

   login(data) {
    return this.httpService.post('users', data, this.headers);
}

}
