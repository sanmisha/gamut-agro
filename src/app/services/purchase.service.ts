import { Injectable } from '@angular/core';
import { HttpService } from './http/http.service';

@Injectable({
  providedIn: 'root'
})
export class PurchaseService {

  headers = {
    Accept: 'application/json',
  };

  constructor(private http: HttpService) { }

  getPurchasesList() {
    return this.http.get('purchases', '', this.headers);
  }

  addPurchases(data) {
    return this.http.post('addPurchases', data, this.headers);
  }

  editPurchases(data, id) {
    return this.http.post('editPurchases', data, this.headers, `/${id}`);
  }
}
