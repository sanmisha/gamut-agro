import { Injectable } from '@angular/core';
import {HttpService} from './http/http.service';

@Injectable({
  providedIn: 'root'
})
export class BranchesService {
  headers = {
    Accept: 'application/json',
  };

  constructor(private http: HttpService) { }

  getBranchesList() {
    return this.http.get('branches', '', this.headers);
  }

  addBranches(data) {
    return this.http.post('addBranches', data, this.headers);
  }

  editBranches(data, id) {
    return this.http.post('editBranches', data, this.headers, `/${id}`);
  }

}
