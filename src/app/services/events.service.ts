import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class EventsService {
  private dataLoginSubject = new Subject();
  private dataUserSubject = new Subject();
  private dataLocationSubject = new Subject();

  constructor() { }

  publishDataLogin(data: any) {
    this.dataLoginSubject.next(data);
}

observeDataLogin() {
    return this.dataLoginSubject;
}

publishDataLocation(data: any) {
    this.dataLocationSubject.next(data);
}

observeDataLocation() {
    return this.dataLocationSubject;
}

publishDataUser(data: any) {
    this.dataUserSubject.next(data);
}

observeDataUser() {
    return this.dataUserSubject;
}
}
