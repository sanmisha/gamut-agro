import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HTTP } from '@ionic-native/http/ngx';
import { Platform } from '@ionic/angular';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class HttpService {
  baseURL = 'http://demo12.sanmishatech.com/api';
  endPoints = {
    items: '/items',
    addItems: '/items/add',
    editItems: '/items/edit',
    grades: '/grades',
    addGrades: '/grades/add',
    editGrades: '/grades/edit',
    branches: '/branches',
    addBranches: '/branches/add',
    editBranches: '/branches/edit',
    warehouses: '/warehouses',
    addWarehouses: '/warehouses/add',
    editWarehouses: '/warehouses/edit',
    customers: '/customers',
    addcustomers: '/customers/add',
    editCustomers: '/customers/edit',
    farmers: '/farmers',
    addFarmers: '/farmers/add',
    editFarmers: '/farmers/edit',
    purchases:'/purchases',
    addPurchases:'/purchases/add',
    editPurchases:'/purchases/edit',
    users: '/users/token'
  };

  constructor(
    private platform: Platform,
    private http: HttpClient,
    private httpNative: HTTP
  ) {}

  getHeaders(headers) {
    let updatedHeaders = {};
    Object.keys(headers).map((k) => {
      updatedHeaders = {
        ...updatedHeaders,
        [k]: headers[k] ? headers[k].toString() : 'null',
      };
    });
    // console.log(JSON.stringify(updatedHeaders));
    return updatedHeaders;
  }

  get(endpoint: string, params: string, headers: {} = {}) {
    console.log(this.getHeaders(headers));
    return new Observable((observer: any) => {
      if (this.platform.is('desktop') || this.platform.is('mobileweb')) {
        this.http
          .get([this.baseURL, this.endPoints[endpoint], params].join(''), {
            responseType: 'text',
            headers: {
              ...this.getHeaders(headers),
            },
          })
          .subscribe(
            (success: any) => {
              observer.next(JSON.parse(success));
            },
            (error) => {
              observer.error(error);
            }
          );
      } else {
        this.httpNative
          .get(
            [this.baseURL, this.endPoints[endpoint], params].join(''),
            {},
            {
              ...this.getHeaders(headers),
            }
          )
          .then((success: any) => {
            observer.next(JSON.parse(success.data));
          })
          .catch((error) => {
            observer.error(error);
          });
      }
    });
  }

  post(endpoint: string, params: {}, headers: {} = {}, queryParameters = '') {
    return new Observable((observer: any) => {
      if (this.platform.is('desktop') || this.platform.is('mobileweb')) {
        this.http
          .post(
            [this.baseURL, this.endPoints[endpoint], queryParameters].join(''),
            params,
            {
              responseType: 'text',
              headers: {
                ...headers,
              },
            }
          )
          .subscribe(
            (success: any) => {
              observer.next(JSON.parse(success));
            },
            (error) => {
              observer.error(error);
            }
          );
      } else {
        this.httpNative.setDataSerializer('json');
        this.httpNative
          .post(
            [this.baseURL, this.endPoints[endpoint], queryParameters].join(''),
            params,
            {
              ...this.getHeaders(headers),
            }
          )
          .then((success: any) => {
            // console.log(success.data);
            observer.next(JSON.parse(success.data));
          })
          .catch((error) => {
            observer.error(error);
          });
      }
    });
  }
  
}
