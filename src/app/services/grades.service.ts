import { Injectable } from '@angular/core';
import {HttpService} from './http/http.service';
@Injectable({
  providedIn: 'root'
})
export class GradesService {
  headers = {
    Accept: 'application/json',
  };
  constructor(private http: HttpService) { }

  getGradesList() {
    return this.http.get('grades', '', this.headers);
  }

  addGrades(data) {
    return this.http.post('addGrades', data, this.headers);
  }

  editGrades(data, id) {
    return this.http.post('editGrades', data, this.headers, `/${id}`);
  }
}
