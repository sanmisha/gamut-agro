import { Injectable } from '@angular/core';
import {HttpService} from './http/http.service';

@Injectable({
  providedIn: 'root'
})
export class FarmersService {
  headers = {
    Accept: 'application/json',
  };

  constructor(private http: HttpService) { }

  getFarmersList() {
    return this.http.get('farmers', '', this.headers);
  }

  addFarmers(data) {
    return this.http.post('addFarmers', data, this.headers);
  }

  editFarmers(data, id) {
    return this.http.post('editFarmers', data, this.headers, `/${id}`);
  }
}
