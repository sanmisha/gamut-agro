import { Injectable } from '@angular/core';
import {HttpService} from './http/http.service';

@Injectable({
  providedIn: 'root'
})
export class CustomersService {
  headers = {
    Accept: 'application/json',
  };

  constructor(private http: HttpService) { }

  getCustomersList() {
    return this.http.get('customers', '', this.headers);
  }
  

  addCustomers(data) {
    return this.http.post('addCustomers', data, this.headers);
  }

  editCustomers(data, id) {
    return this.http.post('editCustomers', data, this.headers, `/${id}`);
  }

}
