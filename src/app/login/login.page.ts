import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import {
  AlertController,
  ToastController,
  LoadingController,
} from '@ionic/angular';
import { AuthService } from '../services/auth.service';
import { EventsService } from '../services/events.service';


@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  private activeTab: any;
    email = '';
    password = '';
    toast: any;
    loading: any;

  constructor(
    private router: Router,
    private alertController: AlertController,
    private toastController: ToastController,
    private loadingController: LoadingController,
    private eventsService:EventsService,
    private authService:AuthService
  ) {}

  ngOnInit() {}


  async presentToast(message) {
    this.toast = await this.toastController.create({
        message,
        duration: 2000
    });
    this.toast.present();
}

async presentLoading(message = 'Please wait...') {
    this.loading = await this.loadingController.create({
        message,
        translucent: true,
        backdropDismiss: false
    });
    await this.loading.present();
}

async stopLoading() {
    await this.loading.dismiss();
}



/**goToRegister() {
    this.router.navigate(['register']);
}**/

goToHome() {
    this.router.navigate(['']);
}

async login() {
    const pushToken = await window.localStorage.getItem('push_token');

    if (this.email === '' || this.password === '') {
        this.presentToast('Please Enter Email and Password');
    } else {
        const params = {
            email: this.email,
            password: this.password,
            uuid: pushToken || '',
        };

        this.presentLoading();

        this.authService.login(params).subscribe((results: any) => {
            if (results.success) {
                const data = results.data.token;
                window.localStorage.setItem('token', data);
                window.localStorage.setItem('login', data);
                window.localStorage.setItem('email', this.email);
                window.localStorage.setItem('password', this.password);

                this.eventsService.publishDataLogin({
                    email: this.email,
                    password: this.password,
                    token: data,
                    isLoggedIn: true
                });

                this.router.navigate(['/dashboard']);

                this.stopLoading();
            } else {
                this.presentToast('Invalid login details. Please check email and password, and try again');
                this.stopLoading();
            }
        }, (err) => {
            this.stopLoading();
            console.error('API Error : ', JSON.stringify(err));
        });
    }
}
}
