import { Component, OnInit } from '@angular/core';
import {
  Validators,
  FormBuilder,
  FormGroup,
} from '@angular/forms';
import { NavigationExtras, Router } from '@angular/router';
import { ModalController } from '@ionic/angular';
import { GradesService } from '../services/grades.service';

@Component({
  selector: 'app-grade-modal',
  templateUrl: './grade-modal.page.html',
  styleUrls: ['./grade-modal.page.scss'],
})
export class GradeModalPage implements OnInit {
  gradeModalForm: FormGroup;
  submitted = false;
  grade;
  onClose;
  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    public viewCtrl: ModalController,
    private service: GradesService,
  ) {
    this.gradeModalForm = this.formBuilder.group({
      grade: ['', Validators.required]
    });
  }

  ngOnInit() {
    if (this.grade) {
      const { grade } = this.grade;
      this.gradeModalForm.setValue({grade});
    }
  }


  onSubmit() {
    console.log(this.gradeModalForm.value.grade);
    if (this.gradeModalForm.value.grade) {
      const data = {
        grade: this.gradeModalForm.value.grade
      };
      if (this.grade) {
        this.service
          .editGrades(data, this.grade.id)
          .subscribe((data: any) => {
            console.log(data);
          });
      } else {
        this.service.addGrades(data).subscribe((data: any) => {
          console.log(data);
        });
      }
      this.dismiss();
    }
  }

  addCrop() {
    this.submitted = true;
    const navigationExtras: NavigationExtras = {
      queryParams: {
        special: JSON.stringify(this.gradeModalForm.value.grade),
      },
    };

    if (!this.gradeModalForm.valid) {
      console.log('Name is required.');
      return false;
    } else {
      this.router.navigate(['grade'], navigationExtras);
      this.dismiss();
    }
  }

  get errorCtr() {
    return this.gradeModalForm.controls;
  }

  dismiss() {
    this.viewCtrl.dismiss().then(() => {
      this.onClose();
    });
  }

}
