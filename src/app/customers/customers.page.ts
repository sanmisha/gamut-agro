import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import {
  ModalController,
  AlertController,
  NavController,
} from '@ionic/angular';
import {CustomersService} from '../services/customers.service';
import {CustomersModalPage} from '../customers-modal/customers-modal.page';

@Component({
  selector: 'app-customers',
  templateUrl: './customers.page.html',
  styleUrls: ['./customers.page.scss'],
})
export class CustomersPage implements OnInit {
  items: any[] = [];
  constructor(
    private router: Router,
    public route: ActivatedRoute,
    public modalController: ModalController,
    public alertController: AlertController,
    public navCtrl: NavController,
    private service:CustomersService
  ) { }

  ngOnInit() {
    this.fetchCustomers();
  }

  fetchCustomers(){
    this.service.getCustomersList().subscribe((data: any) => {
      console.log(data);
      const { customers } = data;
      this.items = customers;
    });
  }

  async openModal(customer = null) {
    const modal = await this.modalController.create({
      component: CustomersModalPage,
      componentProps: {
        customer,
        onClose: () => {
          setTimeout(() => this.fetchCustomers(), 100);
        },
      },
    });
    return await modal.present();
  }
  async deleteCustomer(i) {
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: 'Are you sure you want to delete this customer?',
      buttons: [
        {
          text: 'YES',
          handler: () => {
            this.items.splice(i, 1);
            console.log('Confirm Ok');
          },
        },
        {
          text: 'NO',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          },
        },
      ],
    });
    await alert.present();
  }

  async editCustomer(customer) {
    this.openModal(customer);
  }
}
