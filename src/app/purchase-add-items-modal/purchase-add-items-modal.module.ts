import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule,ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PurchaseAddItemsModalPageRoutingModule } from './purchase-add-items-modal-routing.module';

import { PurchaseAddItemsModalPage } from './purchase-add-items-modal.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    PurchaseAddItemsModalPageRoutingModule
  ],
  declarations: [PurchaseAddItemsModalPage]
})
export class PurchaseAddItemsModalPageModule {}
