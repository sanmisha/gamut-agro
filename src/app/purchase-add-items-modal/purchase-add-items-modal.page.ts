import { Component,  OnInit} from '@angular/core';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import {  NavigationExtras} from '@angular/router';
import { ModalController } from '@ionic/angular';
import { ItemService } from '../services/items.service';
import { NavParams } from '@ionic/angular';
import { PurchaseModalPage } from '../purchase-modal/purchase-modal.page';

@Component({
  selector: 'app-purchase-add-items-modal',
  templateUrl: './purchase-add-items-modal.page.html',
  styleUrls: ['./purchase-add-items-modal.page.scss'],
})
export class PurchaseAddItemsModalPage implements OnInit {
  submitForm: FormGroup;
  submitted = false;
  item_name:any[]=[];
  items: any[] = [];
  quantity;
  rate;
  amount;
  onClose;
  
  
  constructor(
    private formBuilder: FormBuilder,
    public viewCtrl: ModalController,
    private modalController: ModalController,
    private itemservice: ItemService,
    public navParams: NavParams
  ) {
    this.submitForm = this.formBuilder.group({
      item_name: ['', Validators.required],
      quantity: ['', Validators.required],
      rate: ['', Validators.required],
      amount: ['', Validators.required],
    });
    
  }

  ngOnInit() {
    this.fetchItems();
    this.calculate();
    
    
    /**if (this.item_name) {
      const { item_name } = this.item_name;
      this.submitForm.setValue({ item_name });
    }
    if (this.quantity) {
      const { quantity } = this.quantity;
      this.submitForm.setValue({ quantity });
    }

    if (this.rate) {
      const { rate } = this.rate;
      this.submitForm.setValue({ rate });
    }**/
  }

  /**setFormValue(){
    this.submitForm.setValue({item_name:this.item_name,quantity:this.quantity,rate:this.rate})
  }**/

  fetchItems() {
    this.itemservice.getItemsList().subscribe((data: any) => {
      console.log(data);
      const { items } = data;
      this.items = items;
    });
  }
  calculate() {
    this.rate=this.submitForm.value.rate;
    this.quantity = this.submitForm.value.quantity;
    this.amount = this.quantity * this.rate;
    this.item_name=this.submitForm.value.item_name;
    
    let navigationExtras: NavigationExtras = {
      state: {
        items: this.items,
        quantity: this.quantity,
        rate: this.rate,
        amount: this.amount,
        item_name: this.item_name,
      },
    };
    
  }

  get errorCtr() {
    return this.submitForm.controls;
  }

  

  logForm() {
    console.log(this.submitForm.value.amount);
    if (this.submitForm.value.amount) {
      const data = {
        amount: this.submitForm.value.amount,
        rate: this.submitForm.value.rate,
        quantity: this.submitForm.value.quantity,
        item_name: this.submitForm.value.item_name,
      };
    }
   
    this.calculate();
    this.dismiss();
  }

  async openModal() {
    const modal = await this.modalController.create({
      component: PurchaseModalPage,
      componentProps: {
        item_name: this.item_name,
        quantity: this.quantity,
        rate: this.rate,
        amount: this.amount
      },
    });

    
    return await modal.present();
  }


  dismiss() {
    this.viewCtrl.dismiss({quantity:this.quantity,rate:this.rate,amount:this.amount}).then(() => {});
  }
}
