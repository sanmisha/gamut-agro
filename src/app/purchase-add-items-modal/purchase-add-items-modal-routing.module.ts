import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PurchaseAddItemsModalPage } from './purchase-add-items-modal.page';

const routes: Routes = [
  {
    path: '',
    component: PurchaseAddItemsModalPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PurchaseAddItemsModalPageRoutingModule {}
