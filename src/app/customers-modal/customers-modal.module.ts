import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule ,ReactiveFormsModule} from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CustomersModalPageRoutingModule } from './customers-modal-routing.module';

import { CustomersModalPage } from './customers-modal.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    CustomersModalPageRoutingModule
  ],
  declarations: [CustomersModalPage]
})
export class CustomersModalPageModule {}
