import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CustomersModalPage } from './customers-modal.page';

const routes: Routes = [
  {
    path: '',
    component: CustomersModalPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CustomersModalPageRoutingModule {}
