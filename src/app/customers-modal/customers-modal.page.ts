import { Component, OnInit } from '@angular/core';
import {
  Validators,
  FormBuilder,
  FormGroup,
  FormControl,
  ReactiveFormsModule,
} from '@angular/forms';
import { NavigationExtras, Router } from '@angular/router';
import { CustomersPage } from '../customers/customers.page';
import { ModalController } from '@ionic/angular';
import { CustomersService } from '../services/customers.service';
@Component({
  selector: 'app-customers-modal',
  templateUrl: './customers-modal.page.html',
  styleUrls: ['./customers-modal.page.scss'],
})
export class CustomersModalPage implements OnInit {
  customerModalForm: FormGroup;
  private customers: CustomersPage;
  submitted = false;
  customer;
  id;
  onClose;
  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    public viewCtrl: ModalController,
    private service: CustomersService
  ) {
    this.customerModalForm = this.formBuilder.group({
      id:['',Validators.required],
      name:['',Validators.required],
      mobile: ['', Validators.required],
      email: ['', Validators.required],
      address: ['', Validators.required],
      city: ['', Validators.required],
      pincode: ['', Validators.required],
      bank: ['', Validators.required],
      branch: ['', Validators.required],
      account_name: ['', Validators.required],
      account_no: ['', Validators.required],
      ifsc: ['', Validators.required],
      modified:['',Validators.required],
      created:['',Validators.required]
    });
  }

  ngOnInit() {
    if (this.id) {
      const { id } = this.id;
      this.customerModalForm.setValue({ id });
    }
  }

  submitForm() {
    console.log(this.customerModalForm.value.customer);
    if (this.customerModalForm.value.customer) {
      const data = {
        customer: this.customerModalForm.value.customer
      };
      if (this.customer) {
        this.service
          .editCustomers(data, this.customer.id)
          .subscribe((data: any) => {
            console.log(data);
          });
      } else {
        this.service.addCustomers(data).subscribe((data: any) => {
          console.log(data);
        });
      }
      this.dismiss();
    }
  }

  addCustomer() {
    this.submitted = true;
    const navigationExtras: NavigationExtras = {
      queryParams: {
        special: JSON.stringify(this.customerModalForm.value.title),
      },
    };

    if (!this.customerModalForm.valid) {
      console.log('Name is required.');
      return false;
    } else {
      this.router.navigate(['customers'], navigationExtras);
      this.dismiss();
    }
  }

  get errorCtr() {
    return this.customerModalForm.controls;
  }

  dismiss() {
    this.viewCtrl.dismiss().then(() => {
      this.onClose();
    });
  }
}
