import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule,ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { BranchesModalPageRoutingModule } from './branches-modal-routing.module';

import { BranchesModalPage } from './branches-modal.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    BranchesModalPageRoutingModule
  ],
  declarations: [BranchesModalPage]
})
export class BranchesModalPageModule {}
