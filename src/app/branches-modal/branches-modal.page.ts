import { Component, OnInit } from '@angular/core';
import {
  Validators,
  FormBuilder,
  FormGroup,
  FormControl,
  ReactiveFormsModule,
} from '@angular/forms';
import { NavigationExtras, Router } from '@angular/router';
import { BranchesPage } from '../branches/branches.page';
import { ModalController } from '@ionic/angular';
import { BranchesService } from '../services/branches.service';
@Component({
  selector: 'app-branches-modal',
  templateUrl: './branches-modal.page.html',
  styleUrls: ['./branches-modal.page.scss'],
})
export class BranchesModalPage implements OnInit {
  branchModalForm: FormGroup;
  private branches:BranchesPage;
  submitted = false;
  branch;
  onClose;
  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    public viewCtrl: ModalController,
    private service:BranchesService
  ) {
    this.branchModalForm = this.formBuilder.group({
      branch: ['', Validators.required]
    });
   }

  ngOnInit() {
    if (this.branch) {
      const { branch } = this.branch;
      this.branchModalForm.setValue({branch});   
    }
  }

  submitForm() {
    console.log(this.branchModalForm.value.branch);
    if (this.branchModalForm.value.branch) {
      const data = {
        branch: this.branchModalForm.value.branch
      };
      if (this.branch) {
        this.service
          .editBranches(data, this.branch.id)
          .subscribe((data: any) => {
            console.log(data);
          });
      } else {
        this.service.addBranches(data).subscribe((data: any) => {
          console.log(data);
        });
      }
      this.dismiss();
    }
    
  }
  addBranch() {
    this.submitted = true;
    const navigationExtras: NavigationExtras = {
      queryParams: {
        special: JSON.stringify(this.branchModalForm.value.title),
      },
    };

    if (!this.branchModalForm.valid) {
      console.log('Name is required.');
      return false;
    } else {
      this.router.navigate(['branches'], navigationExtras);
      this.dismiss();
    }
  }

  get errorCtr() {
    return this.branchModalForm.controls;
  }

  dismiss() {
    this.viewCtrl.dismiss().then(() => {
      this.onClose();
    });
  }

}
