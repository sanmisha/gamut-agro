import { Component, OnInit } from '@angular/core';
import { Validators, FormBuilder, FormGroup} from '@angular/forms';
import { NavigationExtras, Router } from '@angular/router';
import { ModalController, NavController } from '@ionic/angular';
import { FarmersService } from '../services/farmers.service';
import { PurchaseAddItemsModalPage } from '../purchase-add-items-modal/purchase-add-items-modal.page';
import { PurchaseService } from '../services/purchase.service';


@Component({
  selector: 'app-purchase-modal',
  templateUrl: './purchase-modal.page.html',
  styleUrls: ['./purchase-modal.page.scss'],
})
export class PurchaseModalPage implements OnInit {
  submitForm: FormGroup;
  submitted = false;
  purchase;
  quantity;
  amount;
  rate;
  item_name: any[] = [];
  purchases: any[] = [];
  items: any[] = [];
  farmers: any[] = [];
  purchase_date: any[] = [];
  purchase_no: any[] = [];
  name:any[]=[];
  farmer:any[]=[];
  
  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    public viewCtrl: ModalController,
    private farmerservice: FarmersService,
    private modalController: ModalController,
    private purchaseService: PurchaseService,
    private navCtrl:NavController
  ) {
    this.submitForm = this.formBuilder.group({
      farmers: ['', Validators.required],
      purchase_no: ['', Validators.required],
      purchase_date: ['', Validators.required],
    });
  }

  ngOnInit() {
    
 this.fetchFarmers();
  }

  fetchPurchase() {
    this.purchaseService.getPurchasesList().subscribe((data: any) => {
      console.log(data);
      const { purchases } = data;
      this.purchases = purchases;
    });
  }

  fetchFarmers() {
    this.farmerservice.getFarmersList().subscribe((data: any) => {
      console.log(data);
      const {farmers}=data;
      this.farmers=farmers;
      console.log(farmers)
       
    
    });
  }

  setValue() {
    //this.farmers = this.submitForm.value.farmers;
    this.purchase_no = this.submitForm.value.purchase_no;
    this.purchase_date = this.submitForm.value.purchase_date;
  }

  /**setFormValue(){
  this.submitForm.setValue({farmers:this.farmers,purchase_no:this.purchase_no,purchase_date:this.purchase_date})
}**/

  onSubmit() {
    console.log(this.purchase);
      /**const data = {
        name: this.submitForm.value.name,
        purchase_date: this.submitForm.value.purchase_date,
        purchase_no: this.submitForm.value.purchase_no,
        
      };
       if (this.purchase) {
        this.purchaseService
          .editPurchases(data, this.purchase.id)
          .subscribe((data: any) => {
            console.log(data);
          });
      } else {
        this.purchaseService.addPurchases(data).subscribe((data: any) => {
          console.log(data);
        });
      
    }**/
    
    this.setValue();
    this.dismiss();
    console.log(this.purchases)
  }

  async openModal() {
    const modal = await this.modalController.create({
      component: PurchaseAddItemsModalPage,
      componentProps: {
        purchase: this.purchase,
        amount: this.amount,
        rate: this.rate,
        quantity: this.quantity,
        item_name: this.item_name,
      },
    });
    modal.onDidDismiss().then((data) => {
      console.log(data);
      this.purchases.push(data);
      console.log(this.purchases);
    });

    return await modal.present();
  }

  addPurchase() {
    this.submitted = true;
    const navigationExtras: NavigationExtras = {
      queryParams: {
        special: JSON.stringify(this.submitForm.value.name)
        ,
      },
    };

    if (!this.submitForm.valid) {
      console.log('Name is required.');
      return false;
    } else {
      this.router.navigate(['purchase'], navigationExtras);
      this.dismiss();
    }
  }

  get errorCtr() {
    return this.submitForm.controls;
  }

  dismiss() {
    this.viewCtrl.dismiss({purchase_no:this.purchase_no,purchase_date:this.purchase_date,farmers:this.farmers}).then(() => {});
  
    }
}
