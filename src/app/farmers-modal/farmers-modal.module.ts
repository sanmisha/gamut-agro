import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule,ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { FarmersModalPageRoutingModule } from './farmers-modal-routing.module';

import { FarmersModalPage } from './farmers-modal.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    FarmersModalPageRoutingModule
  ],
  declarations: [FarmersModalPage]
})
export class FarmersModalPageModule {}
