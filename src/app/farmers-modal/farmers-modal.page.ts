import { Component, OnInit } from '@angular/core';
import {
  Validators,
  FormBuilder,
  FormGroup,
  FormControl,
  ReactiveFormsModule,
} from '@angular/forms';
import { NavigationExtras, Router } from '@angular/router';
import { FarmersPage } from '../farmers/farmers.page';
import { ModalController } from '@ionic/angular';
import { FarmersService } from '../services/farmers.service';
@Component({
  selector: 'app-farmers-modal',
  templateUrl: './farmers-modal.page.html',
  styleUrls: ['./farmers-modal.page.scss'],
})
export class FarmersModalPage implements OnInit {
  farmerModalForm: FormGroup;
  private farmers: FarmersPage;
  submitted = false;
  farmer;
  name;
  onClose;
  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    public viewCtrl: ModalController,
    private service: FarmersService
  ) {
    this.farmerModalForm = this.formBuilder.group({
      id:['',Validators.required],
      farmer: ['', Validators.required],
      email: ['', Validators.required],
      mobile: ['', Validators.required],
      address: ['', Validators.required],
      city: ['', Validators.required],
      pincode: ['', Validators.required],
      bank: ['', Validators.required],
      branch: ['', Validators.required],
      account_name: ['', Validators.required],
      account_no: ['', Validators.required],
      ifsc: ['', Validators.required],
      modified:['',Validators.required],
      created:['',Validators.required]
    });
   }

  ngOnInit() {
    if (this.name) {
      const { name } = this.name;
      this.farmerModalForm.setValue({name});   
    }
  }
  submitForm() {
    console.log(this.farmerModalForm.value.farmer);
    if (this.farmerModalForm.value.farmer) {
      const data = {
        farmer: this.farmerModalForm.value.farmer,
        email: this.farmerModalForm.value.email,
        mobile: this.farmerModalForm.value.mobile,
        address: this.farmerModalForm.value.address,
        city: this.farmerModalForm.value.city,
        pincode: this.farmerModalForm.value.pincode,
        bank: this.farmerModalForm.value.bank,
        branch: this.farmerModalForm.value.branch,
        account_name: this.farmerModalForm.value.account_name,
        ifsc: this.farmerModalForm.value.ifsc,
      };
      if (this.farmer) {
        this.service
          .editFarmers(data, this.farmer.id)
          .subscribe((data: any) => {
            console.log(data);
          });
      } else {
        this.service.addFarmers(data).subscribe((data: any) => {
          console.log(data);
        });
      }
      this.dismiss();
    }
  }

  addFarmer() {
    this.submitted = true;
    const navigationExtras: NavigationExtras = {
      queryParams: {
        special: JSON.stringify(this.farmerModalForm.value.title),
      },
    };

    if (!this.farmerModalForm.valid) {
      console.log('Name is required.');
      return false;
    } else {
      this.router.navigate(['farmers'], navigationExtras);
      this.dismiss();
    }
  }

  get errorCtr() {
    return this.farmerModalForm.controls;
  }

  dismiss() {
    this.viewCtrl.dismiss().then(() => {
      this.onClose();
    });
  }
}
