import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FarmersModalPage } from './farmers-modal.page';

const routes: Routes = [
  {
    path: '',
    component: FarmersModalPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class FarmersModalPageRoutingModule {}
