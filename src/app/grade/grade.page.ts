import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import {
  ModalController,
  AlertController,
  NavController,
} from '@ionic/angular';
import { GradesService } from '../services/grades.service';
import { GradeModalPage } from '../grade-modal/grade-modal.page';

@Component({
  selector: 'app-grade',
  templateUrl: './grade.page.html',
  styleUrls: ['./grade.page.scss'],
})
export class GradePage implements OnInit {
  items: any[] = [];

  constructor(
    private router: Router,
    public route: ActivatedRoute,
    public modalController: ModalController,
    public alertController: AlertController,
    public navCtrl: NavController,
    private service: GradesService
  ) {}

  ngOnInit() {
    this.fetchGrades();
  }

  fetchGrades() {
    this.service.getGradesList().subscribe((data: any) => {
      console.log(data);
      const { grades } = data;
      this.items = grades;
    });
  }

  async openModal(grade = null) {
    const modal = await this.modalController.create({
      component: GradeModalPage,
      componentProps: {
        grade,
        onClose: () => {
          setTimeout(() => this.fetchGrades(), 100);
        },
      },
    });
    return await modal.present();
  }

  async deleteGrade(i) {
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: 'Are you sure you want to delete this grade?',
      buttons: [
        {
          text: 'YES',
          handler: () => {
            this.items.splice(i, 1);
            console.log('Confirm Ok');
          },
        },
        {
          text: 'NO',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          },
        },
      ],
    });
    await alert.present();
  }

  async editGrade(grade) {
    this.openModal(grade);
  }
}
