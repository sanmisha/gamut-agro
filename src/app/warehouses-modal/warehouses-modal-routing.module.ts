import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { WarehousesModalPage } from './warehouses-modal.page';

const routes: Routes = [
  {
    path: '',
    component: WarehousesModalPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class WarehousesModalPageRoutingModule {}
