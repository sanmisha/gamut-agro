import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule ,ReactiveFormsModule} from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { WarehousesModalPageRoutingModule } from './warehouses-modal-routing.module';

import { WarehousesModalPage } from './warehouses-modal.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    WarehousesModalPageRoutingModule
  ],
  declarations: [WarehousesModalPage]
})
export class WarehousesModalPageModule {}
