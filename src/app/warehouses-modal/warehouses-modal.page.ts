import { Component, OnInit } from '@angular/core';
import {
  Validators,
  FormBuilder,
  FormGroup
} from '@angular/forms';
import { NavigationExtras, Router } from '@angular/router';
import { WarehousesPage} from '../warehouses/warehouses.page';
import { ModalController } from '@ionic/angular';
import { WarehousesService } from '../services/warehouses.service';
import {BranchesService} from '../services/branches.service';

@Component({
  selector: 'app-warehouses-modal',
  templateUrl: './warehouses-modal.page.html',
  styleUrls: ['./warehouses-modal.page.scss'],
})
export class WarehousesModalPage implements OnInit {
  submitForm: FormGroup;
  submitted = false;
  warehouse;
  branch_id;
  onClose;
  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    public viewCtrl: ModalController,
    private warehousesService: WarehousesService,
    private service:BranchesService
  ) { 
    this.submitForm = this.formBuilder.group({
      //branch_id: ['', Validators.required],
      warehouse: ['', Validators.required],
      
    });
  }

  ngOnInit() {
    this.fetchBranches();
    this.fetchWarehouses();
    console.log(this.warehouse);
   
    console.log(this.branch_id)
    
    if (this.warehouse) {
      const { warehouse } = this.warehouse;
      this.submitForm.setValue({warehouse});
    }
    if(this.branch_id){
      const {branch_id}=this.branch_id;
      this.submitForm.setValue({branch_id})
    }
    
  }

  fetchBranches(){
    this.service.getBranchesList().subscribe((data: any) => {
      console.log(data);
      const {branches}=data;
      this.branch_id=branches;
      console.log(branches)
     
    });
  }
  fetchWarehouses(){
    this.warehousesService.getWarehousesList().subscribe((data: any) => {
      console.log(data);
      const {warehouse}=data;
      this.warehouse=warehouse;
      console.log(warehouse)
    });
  }
   
  logForm() {
    console.log(this.submitForm.value.warehouse);
    if (this.submitForm.value.warehouse) {
      const data = {
        warehouse: this.submitForm.value.warehouse,
        branch_id: this.submitForm.value.branch_id
      };
      if (this.warehouse) {
        this.warehousesService
          .editWarehouses(data, this.warehouse.id)
          .subscribe((data: any) => {
            console.log(data);
          });
      } else {
        this.warehousesService.addWarehouses(data).subscribe((data: any) => {
          console.log(data);
        });
      }
    }
  }

  addWarehouse() {
    this.submitted = true;
    const navigationExtras: NavigationExtras = {
      queryParams: {
        special: JSON.stringify(this.submitForm.value.title),
      },
    };

    if (!this.submitForm.valid) {
      console.log('Name is required.');
      return false;
    } else {
      this.router.navigate(['warehouses'], navigationExtras);
      this.dismiss();
    }
  }

  get errorCtr() {
    return this.submitForm.controls;
  }
  

  dismiss() {
    this.viewCtrl.dismiss().then(() => {
      this.onClose();
    });
  }


}
