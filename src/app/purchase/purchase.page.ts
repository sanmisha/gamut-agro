import { Component, OnInit} from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import {
  ModalController,
  AlertController,
  NavController,
} from '@ionic/angular';
import { PurchaseModalPage } from '../purchase-modal/purchase-modal.page';
import { PurchaseService } from '../services/purchase.service';
import {ItemService} from '../services/items.service';



@Component({
  selector: 'app-purchase',
  templateUrl: './purchase.page.html',
  styleUrls: ['./purchase.page.scss'],
})
export class PurchasePage implements OnInit {
  
  purchases: any[] = [];
  items:any[]=[];
  quantity;
  purchase_no:any[]=[];
  purchase_date:any[]=[];
  farmers:any[]=[];
  name:any[]=[];
  farmer:any[]=[];
 
  constructor(
    public route: ActivatedRoute,
    public modalController: ModalController,
    public alertController: AlertController,
    public navCtrl: NavController,
    private service: PurchaseService,
    private itemService: ItemService,
     
  ) {}

  
  
  
  ngOnInit() {
    this.fetchPurchases();
    console.log(this.farmer);
  }
  

  fetchPurchases() {
    this.service.getPurchasesList().subscribe((data: any) => {
      console.log(data);
      const { purchases } = data;
      this.purchases = purchases;
    
    });
  }

  fetchItems() {
    this.itemService.getItemsList().subscribe((data: any) => {
      console.log(data);
      const { items } = data;
      this.items = items;
    });
  }

  async openModal(purchase = null) {
    const modal = await this.modalController.create({
      component: PurchaseModalPage,
      componentProps: {
        purchase:this.purchases,
        purchase_no:this.purchase_no,
        purchase_date:this.purchase_date,
        farmers:this.farmers,
        farmer:this.farmer
      
      },
    });
    modal.onDidDismiss().then((data) => {
      
      console.log(data)
     // this.purchases.push(this.purchases,data);
     console.log(this.purchases);
     
     
    });
    return await modal.present();
  }
 

  async deletePurchase(i) {
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: 'Are you sure you want to delete this purchase?',
      buttons: [
        {
          text: 'YES',
          handler: () => {
            this.purchases.splice(i, 1);
            console.log('Confirm Ok');
          },
        },
        {
          text: 'NO',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          },
        },
      ],
    });
    await alert.present();
  }

  async editPurchase(purchase) {
    this.openModal(purchase);
  }
}
