import { Component, OnInit,ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import {
  ModalController,
  AlertController,
  NavController,
} from '@ionic/angular';
import { WarehousesService } from '../services/warehouses.service';
import { WarehousesModalPage } from '../warehouses-modal/warehouses-modal.page';
import {BranchesService} from '../services/branches.service';
@Component({
  selector: 'app-warehouses',
  templateUrl: './warehouses.page.html',
  styleUrls: ['./warehouses.page.scss'],
})
export class WarehousesPage implements OnInit {
  items: any[] = [];
  constructor(private router: Router,
    public route: ActivatedRoute,
    public modalController: ModalController,
    public service: WarehousesService,
    public alertController: AlertController,
    public branchService:BranchesService,
    public navCtrl: NavController) { }

  ngOnInit() {
    this.fetchWarehouses();
  }

  fetchWarehouses(){
    this.service.getWarehousesList().subscribe((data: any) => {
      console.log(data);
      const { warehouses } = data;
      this.items = warehouses;
    });
  }

  async openModal(warehouse = null) {
    const modal = await this.modalController.create({
      component: WarehousesModalPage,
      componentProps: {
        warehouse,
        onClose: () => {
          setTimeout(() => this.fetchWarehouses(), 100);
        },
      },
    });
    return await modal.present();
  }

  async deleteWarehouse(i) {
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: 'Are you sure you want to delete this warehouse?',
      buttons: [
        {
          text: 'YES',
          handler: () => {
            this.items.splice(i, 1);
            console.log('Confirm Ok');
          },
        },
        {
          text: 'NO',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          },
        },
      ],
    });
    await alert.present();
  }

  async editWarehouse(warehouse) {
    this.openModal(warehouse);
  }
}
