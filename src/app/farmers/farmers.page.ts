import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import {
  ModalController,
  AlertController,
  NavController,
} from '@ionic/angular';
import {FarmersService} from '../services/farmers.service';
import {FarmersModalPage} from '../farmers-modal/farmers-modal.page';

@Component({
  selector: 'app-farmers',
  templateUrl: './farmers.page.html',
  styleUrls: ['./farmers.page.scss'],
})
export class FarmersPage implements OnInit {
  items: any[] = [];
  constructor(private router: Router,
    public route: ActivatedRoute,
    public modalController: ModalController,
    public alertController: AlertController,
    public navCtrl: NavController,
    private service:FarmersService) { }

  ngOnInit() {
    this.fetchFarmers();
  }

  fetchFarmers(){
    this.service.getFarmersList().subscribe((data: any) => {
      console.log(data);
      const { farmers } = data;
      this.items = farmers;
    });
  }

  async openModal(farmer = null) {
    const modal = await this.modalController.create({
      component: FarmersModalPage,
      componentProps: {
        farmer,
        onClose: () => {
          setTimeout(() => this.fetchFarmers(), 100);
        },
      },
    });
    return await modal.present();
  }
  async deleteFarmer(i) {
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: 'Are you sure you want to delete this farmer?',
      buttons: [
        {
          text: 'YES',
          handler: () => {
            this.items.splice(i, 1);
            console.log('Confirm Ok');
          },
        },
        {
          text: 'NO',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          },
        },
      ],
    });
    await alert.present();
  }

  async editFarmer(farmer) {
    this.openModal(farmer);
  }
}
