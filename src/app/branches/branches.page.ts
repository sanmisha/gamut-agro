import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import {
  ModalController,
  AlertController,
  NavController,
} from '@ionic/angular';
import {BranchesService} from '../services/branches.service';
import {BranchesModalPage} from '../branches-modal/branches-modal.page';

@Component({
  selector: 'app-branches',
  templateUrl: './branches.page.html',
  styleUrls: ['./branches.page.scss'],
})
export class BranchesPage implements OnInit {
  items: any[] = [];

  constructor(private router: Router,
    public route: ActivatedRoute,
    public modalController: ModalController,
    public alertController: AlertController,
    public navCtrl: NavController,
    private service:BranchesService) { }

  ngOnInit() {
    this.fetchBranches();
  }

  fetchBranches(){
    this.service.getBranchesList().subscribe((data: any) => {
      console.log(data);
      const { branches } = data;
      this.items = branches;
    });
  }

  async openModal(branch = null) {
    const modal = await this.modalController.create({
      component: BranchesModalPage,
      componentProps: {
        branch,
        onClose: () => {
          setTimeout(() => this.fetchBranches(), 100);
        },
      },
    });
    return await modal.present();
  }
  async deleteBranch(i) {
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: 'Are you sure you want to delete this branch?',
      buttons: [
        {
          text: 'YES',
          handler: () => {
            this.items.splice(i, 1);
            console.log('Confirm Ok');
          },
        },
        {
          text: 'NO',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          },
        },
      ],
    });
    await alert.present();
  }

  async editBranch(branch) {
    this.openModal(branch);
  }
}
