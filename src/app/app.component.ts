import { Component } from '@angular/core';
import { LoginPage } from './login/login.page';
import { AlertController } from '@ionic/angular';
import { Router } from '@angular/router';
@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent {
  public appPages = [
    { title: 'Home', url: '/dashboard', icon: 'home' },
    { title: 'Grade', url: '/grade', icon: 'archive' },
    { title: 'Items', url: '/items', icon: 'cart' },
    { title: 'Branches', url: '/branches', icon: 'cart' },
    { title: 'Warehoues', url: '/warehouses', icon: 'cart' },
    { title: 'Customers', url: '/customers', icon: 'cart' },
    { title: 'Farmers', url: '/farmers', icon: 'cart' },
    { title: 'Purchase', url: '/purchase', icon: 'cart' }
  ];
  public labels = ['Family', 'Friends', 'Notes', 'Work', 'Travel', 'Reminders'];
  constructor(
    private router: Router,
    public alertController: AlertController
  ) {

  }

  redirectTo(link) {
    this.router.navigateByUrl(link);
  }

  async openLogout() {
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: 'Are you sure you want to log out?',
      buttons: [{
        text: 'YES',
        handler: () => {
          this.router.navigate(['/'])
          console.log('Confirm Ok');
        }
      },
      {
        text: 'NO',
        role: 'cancel',
        handler: () => {
          console.log('Cancel clicked');
        }

      }]
    });
    await alert.present();
  }
}
