import { Component, OnInit } from '@angular/core';
import {
  Validators,
  FormBuilder,
  FormGroup
} from '@angular/forms';
import { NavigationExtras, Router } from '@angular/router';
import { ModalController } from '@ionic/angular';
import { ItemService } from '../services/items.service';
import { GradesService } from '../services/grades.service';
import { ItemsPage } from '../items/items.page';

@Component({
  selector: 'app-items-modal',
  templateUrl: './items-modal.page.html',
  styleUrls: ['./items-modal.page.scss'],
})
export class ItemsModalPage implements OnInit {
  itemsModalForm: FormGroup;
  submitted = false;
  item;
  grades:[];
  grade:any;
  onClose;
  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    public viewCtrl: ModalController,
    private itemService: ItemService,
    private service: GradesService
  ) {
    this.itemsModalForm = this.formBuilder.group({
      item_name: ['', Validators.required],
      //grade_id: ['', Validators.required],
      //grade:['',Validators.required]
    });
  }

  ngOnInit() {
    console.log(this.item);
    this.fetchGrades();
    
    if (this.item) {
      const { item_name } = this.item;
      this.itemsModalForm.setValue({ item_name });
    }

    if (this.grade) {
      const { grade } = this.grade;
      this.itemsModalForm.setValue({ grade });
    } 
    
  }


  fetchGrades() {
    this.service.getGradesList().subscribe((data: any) => {
      console.log(data);
      const { grades } = data;
      this.grades = grades;
    });
  }

  async openModal() {
    const modal = await this.viewCtrl.create({
      component: ItemsPage,
      componentProps: {
        grade:this.grades
      },
    });

    
    return await modal.present();
  }
  onSubmit() {
    console.log(this.itemsModalForm.value.item_name);
    if (this.itemsModalForm.value.item_name) {
      const data = {
        item_name: this.itemsModalForm.value.item_name,
      };
      if (this.item) {
        this.itemService
          .editItems(data, this.item.id)
          .subscribe((data: any) => {
            console.log(data);
          });
      } else {
        this.itemService.addItems(data).subscribe((data: any) => {
          console.log(data);
        });
      }
      this.dismiss();
    }
  }
  

  addItem() {
    this.submitted = true;
    const navigationExtras: NavigationExtras = {
      queryParams: {
        special: JSON.stringify(this.itemsModalForm.value.item),
      },
    };

    if (!this.itemsModalForm.valid) {
      console.log('Name is required.');
      return false;
    } else {
      this.router.navigate(['items'], navigationExtras);
      this.dismiss();
    }
  }

  get errorCtr() {
    return this.itemsModalForm.controls;
  }

  dismiss() {
    this.viewCtrl.dismiss({grades:this.grades}).then(() => {
      this.onClose();
    });
  }
}
