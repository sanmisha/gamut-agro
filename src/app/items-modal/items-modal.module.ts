import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule,ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ItemsModalPageRoutingModule } from './items-modal-routing.module';

import { ItemsModalPage } from './items-modal.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReactiveFormsModule,
    ItemsModalPageRoutingModule
  ],
  declarations: [ItemsModalPage]
})
export class ItemsModalPageModule {}
