import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'login',
    pathMatch: 'full'
  },
  {
    path: 'login',
    loadChildren: () => import('./login/login.module').then( m => m.LoginPageModule)
  },
  {
    path: 'items',
    loadChildren: () => import('./items/items.module').then( m => m.ItemsPageModule)
  },
  {
    path: 'items-modal',
    loadChildren: () => import('./items-modal/items-modal.module').then( m => m.ItemsModalPageModule)
  },
  {
    path: 'dashboard',
    loadChildren: () => import('./dashboard/dashboard.module').then( m => m.DashboardPageModule)
  },
  {
    path: 'grade',
    loadChildren: () => import('./grade/grade.module').then( m => m.GradePageModule)
  },
  {
    path: 'grade-modal',
    loadChildren: () => import('./grade-modal/grade-modal.module').then( m => m.GradeModalPageModule)
  },
  {
    path: 'branches',
    loadChildren: () => import('./branches/branches.module').then( m => m.BranchesPageModule)
  },
  {
    path: 'branches-modal',
    loadChildren: () => import('./branches-modal/branches-modal.module').then( m => m.BranchesModalPageModule)
  },
  {
    path: 'warehouses',
    loadChildren: () => import('./warehouses/warehouses.module').then( m => m.WarehousesPageModule)
  },
  {
    path: 'warehouses-modal',
    loadChildren: () => import('./warehouses-modal/warehouses-modal.module').then( m => m.WarehousesModalPageModule)
  },
  {
    path: 'customers',
    loadChildren: () => import('./customers/customers.module').then( m => m.CustomersPageModule)
  },
  {
    path: 'customers-modal',
    loadChildren: () => import('./customers-modal/customers-modal.module').then( m => m.CustomersModalPageModule)
  },
  {
    path: 'farmers',
    loadChildren: () => import('./farmers/farmers.module').then( m => m.FarmersPageModule)
  },
  {
    path: 'farmers-modal',
    loadChildren: () => import('./farmers-modal/farmers-modal.module').then( m => m.FarmersModalPageModule)
  },
  {
    path: 'purchase',
    loadChildren: () => import('./purchase/purchase.module').then( m => m.PurchasePageModule)
  },
  {
    path: 'purchase-modal',
    loadChildren: () => import('./purchase-modal/purchase-modal.module').then( m => m.PurchaseModalPageModule)
  },
  {
    path: 'purchase-add-items-modal',
    loadChildren: () => import('./purchase-add-items-modal/purchase-add-items-modal.module').then( m => m.PurchaseAddItemsModalPageModule)
  }


];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
