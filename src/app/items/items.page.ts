import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import {
  ModalController,
  AlertController,
  NavController,
} from '@ionic/angular';
import { ItemService } from '../services/items.service';
import { ItemsModalPage } from '../items-modal/items-modal.page';
@Component({
  selector: 'app-items',
  templateUrl: './items.page.html',
  styleUrls: ['./items.page.scss'],
})
export class ItemsPage implements OnInit {
  items: any[] = [];
  grade:any[]=[];
  constructor(
    private router: Router,
    public route: ActivatedRoute,
    public modalController: ModalController,
    public service: ItemService,
    public alertController: AlertController,
    public navCtrl: NavController
  ) {}

  ngOnInit() {
    this.fetchItems();
    console.log(this.grade);
  }

  fetchItems() {
    this.service.getItemsList().subscribe((data: any) => {
      console.log(data);
      const { items } = data;
      this.items = items;
    });
  }
  

  async openModal(item = null,grade=null) {
    const modal = await this.modalController.create({
      component: ItemsModalPage,
      componentProps: {
        item,grade,
        onClose: () => {
          setTimeout(() => this.fetchItems(), 100);
        },
      },
    });
    return await modal.present();
  }

  

  async deleteItem(i) {
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: 'Are you sure you want to delete this item?',
      buttons: [
        {
          text: 'YES',
          handler: () => {
            this.items.splice(i, 1);
            console.log('Confirm Ok');
          },
        },
        {
          text: 'NO',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          },
        },
      ],
    });
    await alert.present();
  }

  async editItem(item,grade) {
    this.openModal(item,grade);
  }
  /**async editGrade(grade) {
    this.openModal(grade);
  }**/
}
